using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLives;
    [SerializeField] private TextMeshProUGUI liveText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI finalCoinsCount;
    [SerializeField] private string firstLevelName = "Level 1";
    private int lives;
    private int coins = 0;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
            lives = startLives;
            coins = 0;
            ShowLives(lives);
            ShowCoins(coins);
    }

    public void LoseLive()
    {
        lives--;
        if (lives <= 0)
        {
            lives = 0;
            GameOver();
        }
        ShowLives(lives);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinsCount.text = coins.ToString();
    }

       
        private void restartGame()
    {
        lives = startLives;
        coins = 0;
        SceneManager.LoadScene(firstLevelName);
        ShowCoins(coins);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
     
        public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins(coins);
    }
    

    private void ShowLives(int amount)
    {
        liveText.text = amount.ToString();
    }
    private void ShowCoins(int amount)
        {
            coinText.text = amount.ToString();
        }
}

