using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private float jumpforce = 3f;
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private string groundLayer = "Ground";
    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;
    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() 
    { 
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flip(playerInput);
        SwitchAnimation(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if(Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }
    }

    private void Move(float direction)
    {
        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpforce);
        playerRigidbody.velocity = jumpVector; 
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }

    private void Flip(float playerInput)
    {
        bool isSpriteFlip = playerSpriteRenderer.flipX;
        if(playerInput < 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        if(playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }

}
